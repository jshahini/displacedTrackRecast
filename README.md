# RECAST: Compressed Electroweak SUSY
Welcome to the RECAST package for the compressed electroweak SUSY analysis using displaced tracks (`ANA-SUSY-2020-04`). This should walk you through everything you need to do in order to reinterpret the world's best analysis. The workflow is designed to run on the dedicated REANA cluster.

### Useful links:
* [Run 2 Compressed Electroweak SUSY twiki](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/HiggsinosRun2)
* [GLANCE entry](https://atlas-glance.cern.ch/atlas/analysis/analyses/details?id=3826)
* [ATLAS RECAST docs](https://recast.docs.cern.ch/)
* [SusySkimHiggsino](https://gitlab.cern.ch/atlas-phys-susy-higgsino/SusySkimHiggsino)
* [HiggsinoFitterSW](https://gitlab.cern.ch/atlas-phys-susy-higgsino/HiggsinoFitterSW)

### Contents
1. [Workflow](#workflow)
2. [Input Parameters](#input-parameters)
3. [Signal Cross-Sections](#signal-cross-sections)
4. [Running on REANA Cluster](#running-on-reana-cluster)
    1. [Setting things up](#setting-things-up)
    2. [Submitting the RECAST job](#submitting-the-recast-job)
    3. [Monitoring the RECAST job](#monitoring-the-recast-job)
    4. [Retrieving the RECAST job results](#retrieving-the-recast-job-results)
    5. [Retrieving logs and files](#retrieving-logs-and-files)
    6. [Deleting RECAST jobs](#deleting-recast-jobs)
    7. [REANA Tips and Tricks](#reana-tips-and-tricks)
5. [To-Do List](#to-do-list)

# Workflow
<img src="yadage_workflow_instance.png" alt="Workflow visualization" width="1000">

Here is a brief description of each step in the workflow:
*  `DxAOD_to_ntuple_mc16a`: Produce signal ntuple from mc16a DxAOD and weight it according to relative luminosity (0.262) and cross-section.
*  `DxAOD_to_ntuple_mc16d`: Produce signal ntuple from mc16d DxAOD and weight it according to relative luminosity (0.318) and cross-section.
*  `DxAOD_to_ntuple_mc16e`: Produce signal ntuple from mc16e DxAOD and weight it according to relative luminosity (0.420) and cross-section.
*  `fetchfiles`: Fetch the background/data histogram cache from EOS.
*  `merge`: Merge the mc16a/d/e ntuples.
*  `fit`: Perform the exclusion fit.


# Input Parameters
The following parameters need to be defined when running RECAST. The default values are defined in `reana.yml`. **Note that only FastSim (AF2) signal samples are supported at the moment.**
*  `prefix`: The eos server used to fetch the input DAODs from `/eos/`.
*  `dxaod_file_mc16a`: The path to the directory containing the signal file(s) for mc16a. This needs to point to a directory that the RECAST/REANA service account can access.
*  `dxaod_file_mc16d`: The path to the directory containing the signal file(s) for mc16d. This needs to point to a directory that the RECAST/REANA service account can access.
*  `dxaod_file_mc16e`: The path to the directory containing the signal file(s) for mc16e. This needs to point to a directory that the RECAST/REANA service account can access.
*  `signal_xsec`: The path to a PMG-style text file containing the DSIDs, cross-sections, generator filter efficiencies, k-factors, and relative uncertainties. This should be a string works with xrootd and point to a directory that the RECAST/REANA service account can access.
*  `n_events`:   The number of events to run over per signal file. Set this to `-1` to run over all events
*  `sys_set`: The set of systematics to run over when producing the ntuples. This should be set to `'DT_allSys_GlobalReduction_SimpleJER'` in order to run over the full set of systematics or `'noSys'` to only produce the nominal trees. 
*  `fit_sys`: The set of systematics to use when running the fit. This should be set to `'AllSys'` to run the fit with all systematics included, or `'NoSys'` to run without any detector or theory systematics
*  `bkg_cache_path`: The path to the background/data histogram cache. This should be a string that works with xrootd and point to a directory that the RECAST/REANA service account can access.

# Signal Cross-Sections
You need to provide the signal DSID, cross-section, k-factor, generator filter efficiency, and relative uncertainty. To do this, make a PMG-style file that looks like this:
<pre>
dataset_number/I:physics_short/C:crossSection/D:genFiltEff/D:kFactor/D:relUncertUP/D:relUncertDOWN/D:generator_name/C:etag/C
510787  MGPy8EG_A14N23LO_N2C1p_151_150p5_150_MET125  0.21717707  3.073698E-01  1.0  0.04336338721  0.0  MadGraph(v.2.9.5.atlas2)+Pythia8(v.306)+EvtGen(v.1.7.0)  e8435
510788  MGPy8EG_A14N23LO_N2C1m_151_150p5_150_MET125  0.12161369  2.969675E-01  1.0  0.06639671264  0.0  MadGraph(v.2.9.5.atlas2)+Pythia8(v.306)+EvtGen(v.1.7.0)  e8435
510789  MGPy8EG_A14N23LO_N1C1p_151_150p5_150_MET125  0.21939618  3.081161E-01  1.0  0.04329690869  0.0  MadGraph(v.2.9.5.atlas2)+Pythia8(v.306)+EvtGen(v.1.7.0)  e8435
510790  MGPy8EG_A14N23LO_N1C1m_151_150p5_150_MET125  0.12248683  2.991294E-01  1.0  0.06632231598  0.0  MadGraph(v.2.9.5.atlas2)+Pythia8(v.306)+EvtGen(v.1.7.0)  e8435
510791  MGPy8EG_A14N23LO_N2N1_151_150p5_150_MET125  0.16996833  3.082907E-01  1.0  0.04698048868  0.0  MadGraph(v.2.9.5.atlas2)+Pythia8(v.306)+EvtGen(v.1.7.0)  e8435
510792  MGPy8EG_A14N23LO_C1C1_151_150p5_150_MET125  0.17205084  3.128729E-01  1.0  0.05028071097  0.0  MadGraph(v.2.9.5.atlas2)+Pythia8(v.306)+EvtGen(v.1.7.0)  e8435
</pre>

# Running on REANA Cluster
### Setting things up
Here are the instructions for running the RECAST workflow on the dedicated REANA cluster. First, log in to lxplus, clone the repository, and set up the REANA client:
<pre>
ssh username@lxplus.cern.ch
git clone https://gitlab.cern.ch/jshahini/displacedTrackRecast.git
cd displacedTrackRecast
source /afs/cern.ch/user/r/reana/public/reana/bin/activate
</pre>

Before running on the REANA cluster, we need to set up some credentials. First, you will need a `REANA_ACCESS_TOKEN`, which you can obtain by opening [https://reana.cern.ch/](https://reana.cern.ch/) in your browser, signing in with your CERN credentials, and navigating to your profile. If you have not created this token before, you will need to request one to be generated at that webpage. Once you have your token, you need to set the following environmant variables, replacing `XXXXXXX` with your actual token:
<pre>
export REANA_SERVER_URL=https://reana.cern.ch/
export REANA_ACCESS_TOKEN=XXXXXXX
</pre>

Next, you need to set up your Kerberos authentication by generating and uploading a keytab file. You can find instructions [at this webpage](http://docs.reana.io/advanced-usage/access-control/kerberos/), but they are reproduced here for convenience. You should replace `username` with yours where appropriate:

<pre>
# Generate the keytab file
ktutil
ktutil:  add_entry -password -p username@CERN.CH -k 1 -e aes256-cts-hmac-sha1-96
Password for username@CERN.CH:
ktutil:  add_entry -password -p username@CERN.CH -k 1 -e arcfour-hmac
Password for username@CERN.CH:
ktutil:  write_kt .keytab
ktutil:  exit
# Upload the keytab file as a secret to the REANA clutster
reana-client secrets-add --env CERN_USER=username --env CERN_KEYTAB=.keytab --file .keytab
</pre>

This Kerberos authentication recipe needs to be repeated every time your previous authentication expires.

### Submitting the RECAST job
Now we're ready to submit the workflow to the REANA cluster. The `-w` argument allows you to apply a name to your workflow, so you should use it to describe the signal you're running on.
<pre>
reana-client run -w myAnalysis
</pre>
If you submit multiple workflows using the same name, they will be indexed like `myAnalysis.1`, `myAnalysis.2`, `myAnalysis.3`, etc.

The above command will submit a job using the input parameters defined in [reana.yml](reana.yml). In order to use the parameters you want, you can either change them directly in `reana.yml` or make a new file and point to it using the `-f` option:
<pre>
cp reana.yml newinput.yml
# Edit newinput.yml to use your new parameters
reana-client run -w myAnalysis -f newinput.yml
</pre>

Now there's an extra complication: it's common within the SUSY group to generate different signal processes separately and so it's possible to have multiple DSIDs per signal point. In the case of the Higgsino model, for example, we are targeting `N2C1p`, `N2C1m`, `N1C1p`, `N1C1m`, `N2N1`, and `C1C1` production, which were generated separately and therefore have different DSIDs. In cases like this, you can simply add more signal samples to the `dxaod_file_mc16a/d/e` input parameters. This list can have any length to account for an arbitrary number of DSIDs and the ntuple production will all run in parallel, so it shouldn't affect the overall runtime. You still only need to provide one cross-section file, but it should contain the relevant information for all DSIDs you're running over. For example, in the Higgsino case, your input parameters could look like this, specifying two DSIDs per signal point:
<pre>
dxaod_file_mc16a: 
  - '/eos/project/r/recast/atlas/ANA-SUSY-2018-16/testdata/displacedTrack/mc16_13TeV.510787.MGPy8EG_A14N23LO_N2C1p_151_150p5_150_MET125.deriv.DAOD_SUSY20.e8435_a875_r9364_p4929'
  - '/eos/project/r/recast/atlas/ANA-SUSY-2018-16/testdata/displacedTrack/mc16_13TeV.510788.MGPy8EG_A14N23LO_N2C1m_151_150p5_150_MET125.deriv.DAOD_SUSY20.e8435_a875_r9364_p4929'
  - '/eos/project/r/recast/atlas/ANA-SUSY-2018-16/testdata/displacedTrack/mc16_13TeV.510789.MGPy8EG_A14N23LO_N1C1p_151_150p5_150_MET125.deriv.DAOD_SUSY20.e8435_a875_r9364_p4929'
  - '/eos/project/r/recast/atlas/ANA-SUSY-2018-16/testdata/displacedTrack/mc16_13TeV.510790.MGPy8EG_A14N23LO_N1C1m_151_150p5_150_MET125.deriv.DAOD_SUSY20.e8435_a875_r9364_p4929'
  - '/eos/project/r/recast/atlas/ANA-SUSY-2018-16/testdata/displacedTrack/mc16_13TeV.510791.MGPy8EG_A14N23LO_N2N1_151_150p5_150_MET125.deriv.DAOD_SUSY20.e8435_a875_r9364_p4929'
  - '/eos/project/r/recast/atlas/ANA-SUSY-2018-16/testdata/displacedTrack/mc16_13TeV.510792.MGPy8EG_A14N23LO_C1C1_151_150p5_150_MET125.deriv.DAOD_SUSY20.e8435_a875_r9364_p4929'
dxaod_file_mc16d: 
  - '/eos/project/r/recast/atlas/ANA-SUSY-2018-16/testdata/displacedTrack/mc16_13TeV.510787.MGPy8EG_A14N23LO_N2C1p_151_150p5_150_MET125.deriv.DAOD_SUSY20.e8435_a875_r10201_p4929'
  - '/eos/project/r/recast/atlas/ANA-SUSY-2018-16/testdata/displacedTrack/mc16_13TeV.510788.MGPy8EG_A14N23LO_N2C1m_151_150p5_150_MET125.deriv.DAOD_SUSY20.e8435_a875_r10201_p4929'
  - '/eos/project/r/recast/atlas/ANA-SUSY-2018-16/testdata/displacedTrack/mc16_13TeV.510789.MGPy8EG_A14N23LO_N1C1p_151_150p5_150_MET125.deriv.DAOD_SUSY20.e8435_a875_r10201_p4929'
  - '/eos/project/r/recast/atlas/ANA-SUSY-2018-16/testdata/displacedTrack/mc16_13TeV.510790.MGPy8EG_A14N23LO_N1C1m_151_150p5_150_MET125.deriv.DAOD_SUSY20.e8435_a875_r10201_p4929'
  - '/eos/project/r/recast/atlas/ANA-SUSY-2018-16/testdata/displacedTrack/mc16_13TeV.510791.MGPy8EG_A14N23LO_N2N1_151_150p5_150_MET125.deriv.DAOD_SUSY20.e8435_a875_r10201_p4929'
  - '/eos/project/r/recast/atlas/ANA-SUSY-2018-16/testdata/displacedTrack/mc16_13TeV.510792.MGPy8EG_A14N23LO_C1C1_151_150p5_150_MET125.deriv.DAOD_SUSY20.e8435_a875_r10201_p4929'
dxaod_file_mc16e: 
  - '/eos/project/r/recast/atlas/ANA-SUSY-2018-16/testdata/displacedTrack/mc16_13TeV.510787.MGPy8EG_A14N23LO_N2C1p_151_150p5_150_MET125.deriv.DAOD_SUSY20.e8435_a875_r10724_p4929'
  - '/eos/project/r/recast/atlas/ANA-SUSY-2018-16/testdata/displacedTrack/mc16_13TeV.510788.MGPy8EG_A14N23LO_N2C1m_151_150p5_150_MET125.deriv.DAOD_SUSY20.e8435_a875_r10724_p4929'
  - '/eos/project/r/recast/atlas/ANA-SUSY-2018-16/testdata/displacedTrack/mc16_13TeV.510789.MGPy8EG_A14N23LO_N1C1p_151_150p5_150_MET125.deriv.DAOD_SUSY20.e8435_a875_r10724_p4929'
  - '/eos/project/r/recast/atlas/ANA-SUSY-2018-16/testdata/displacedTrack/mc16_13TeV.510790.MGPy8EG_A14N23LO_N1C1m_151_150p5_150_MET125.deriv.DAOD_SUSY20.e8435_a875_r10724_p4929'
  - '/eos/project/r/recast/atlas/ANA-SUSY-2018-16/testdata/displacedTrack/mc16_13TeV.510791.MGPy8EG_A14N23LO_N2N1_151_150p5_150_MET125.deriv.DAOD_SUSY20.e8435_a875_r10724_p4929'
  - '/eos/project/r/recast/atlas/ANA-SUSY-2018-16/testdata/displacedTrack/mc16_13TeV.510792.MGPy8EG_A14N23LO_C1C1_151_150p5_150_MET125.deriv.DAOD_SUSY20.e8435_a875_r10724_p4929'
</pre>
For the above example, you can see in [this diagram](yadage_workflow_instance_6DSIDs.png) how there are now six instances of the `DxAOD_to_ntuple_mc16a/d/e` steps (one per DSID) that run in parallel.

### Monitoring the RECAST job
Now that the RECAST job is submitted, you can monitor the status either using the UI at [https://reana.cern.ch/](https://reana.cern.ch/) or the command line. For example, you can get the status of the workflow and see what workspace files exist with this:

<pre>
# Check the status of the workflow
reana-client status -w myAnalysis
# List workspace files
reana-client ls -w myAnalysis
</pre>

Eventually, the job status will be `finished` and you can proceed to retrieving the results.


### Retrieving the RECAST job results
Once the job is done, you can retrieve the fit results with this:
<pre>
reana-client download -w myAnalysis -o myOutputDirectory
</pre>

This will save the `CLs` values to `myOutputDirectory/fit/results.txt`. They should look something like this:
<pre>
cat myOutputDirectory/fit/results.txt
Observed CLs = 0.02535718
Expected CLs = 0.02529856
</pre>

Additionally, this will save the usual `json` files that store all of the fit results needed to draw contours with the +/-1 sigma bands, cross-section variations, etc. They will be saved to the same place:
<pre>
myOutputDirectory/fit/fit_results_fixSigXSecNominal__1_harvest_list.json
myOutputDirectory/fit/fit_results_fixSigXSecUp__1_harvest_list.json
myOutputDirectory/fit/fit_results_fixSigXSecDown__1_harvest_list.json
</pre>

### Retrieving logs and files
When scrutinizing the results and debugging the workflow, it can very helpful to look at the logs from the various steps and the ntuples that get produced. All of the relevant logs and files can be viewed/downloaded using the UI at [https://reana.cern.ch/](https://reana.cern.ch/). But it's also nice to do this via the command line, which is described below.

The logs can be fetched using `reana-client logs` and you can specify the step you're interested in using `--filter`. This example will dump the log for the `fit` step into a file called `fit.log`:
<pre>
reana-client logs -w myAnalysis --filter step=fit > fit.log
</pre>

Since the number of `DxAOD_to_ntuple_mc16X` steps depends on the number of input DSIDs, you will need to specify the index of the DSID if you want to retrieve these logs. This index starts at `0`, corresponding to the first input dataset specificed by your `dxaod_file_mc16X` parameter. So if you want the `DxAOD_to_ntuple_mc16a` log for the first DSID you specified with `dxaod_file_mc16a`, you can fetch it like this:
<pre>
reana-client logs -w myAnalysis --filter step=DxAOD_to_ntuple_mc16a_0 > DxAOD_to_ntuple_mc16a_0.log
</pre>
Note that, as of right now, you can only view the logs for a given step after that step has either completed or failed. This may change in the future with further REANA improvements, though.


It's also useful for debugging to look at the ntuples that get produced. If you want the final merged ntuple, you can retrieve it from the `merge` step with this:
<pre>
reana-client download merge/merged_ntuple.root -o myOutputDirectory -w myAnalysis
</pre>

You can also retrieve the ntuple for a specific mc campaign and input DSID. For example, the mc16a ntuple for the first input DSID can be fetched with this:
<pre>
reana-client download DxAOD_to_ntuple_mc16a_0/ntuple_mc16a.root -o myOutputDirectory -w myAnalysis
</pre>

If you want to download all of the fit results produced by `HistFitter`, you can do that with this:
<pre>
reana-client ls -w myAnalysis  | grep ^fit | awk '{print $1}' | xargs reana-client download -w myAnalysis -o myOutputDirectory
</pre>
This will save the `results`, `config`, and `data` outputs from `HistFitter` to `myOutputDirectory/fit/`.

### Deleting RECAST jobs
Once you have your results/files/logs locally and you're satisfied with everything, you can delete your workflow with this:
<pre>
reana-client delete -w myAnalysis --include-workspace
</pre>
This will also delete the files in the workspace, so you will no longer be able to retreive anything from this job, but it will free up disk space.

In order to delete all records of the workflow (including the workspace) so that it doesn't show up with e.g. `reana-client list`, you can use the `--include-records` option:
<pre>
reana-client delete -w myAnalysis --include-records
</pre>

If you want to delete all runs of a given workflow (`myAnalysis.1`, `myAnalysis.2`, `myAnalysis.3`, etc.), you can use the `--include-all-runs` option. For example:
<pre>
reana-client delete -w myAnalysis --include-workspace --include-all-runs
</pre>

### REANA Tips and Tricks
For more information about the REANA-client version that is currently deployed on the REANA cluster, see the documentation here:
[https://reana-client.readthedocs.io/en/latest/](https://reana-client.readthedocs.io/en/latest/)
 
Here are some handy commands for inspecting workflows on the REANA cluster. At the time of writing, the REANA cluster is running `v0.8.0`, which has a nice [blog post](https://blog.reana.io/posts/2021/release-0.8.0/) introducing some new features beyond the basic tips described below. So I'd recommend checking that out as well.

List the sizes of all files in the workspace:
<pre>
reana-client du -w myAnalysis
</pre>

List all workflows and sessions:
<pre>
reana-client list
</pre>

Show the diff between two workflows:
<pre>
reana-client diff myAnalysis.1 myAnalysis.2
# Ignore differences in contents of workspace files:
reana-client diff myAnalysis.1 myAnalysis.2 -q
</pre>


# To-Do List
*  Allow for user-defined pdf/scale variations for signal
*  Add support for FullSim signal samples